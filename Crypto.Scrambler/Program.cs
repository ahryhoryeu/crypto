﻿using System;
using System.Collections.Generic;
using System.IO;
using Crypto.Utils.Core;

namespace Crypto.Scrambler
{
    class Program
    {
        private const int RegisterLength = 8;
        private const string SourceFileName = "exampleText.txt";
        private const string EncryptedTextFilename = "encrypted.txt";
        private const string DecryptedTextFilename = "decrypted.txt";
        private const int BlockSize = 32;

        static void Main(string[] args)
        {
            var sequenceGenerator = new RandomBitGenerator();
            BitArray initialRegister = sequenceGenerator.GetRandomBitArray(RegisterLength);
            BitArray key = sequenceGenerator.GetRandomBitArray(RegisterLength);

            var senderScrambler = new global::Scrambler.Scramble.Scrambler(key, initialRegister);
            global::Scrambler.Scramble.Scrambler receiverScrambler = senderScrambler.Clone();

            var bitBlockReader = new BitBlockReader(SourceFileName);
            bool hasMoreBlocks = true;
            var encryptedBits = new List<Bit>(bitBlockReader.OverallSize);
            var decryptedBits = new List<Bit>(bitBlockReader.OverallSize);

            while (hasMoreBlocks)
            {
                BitArray block;
                hasMoreBlocks = bitBlockReader.GetBlock(BlockSize, out block);
                foreach (var bit in block)
                {
                    Bit encryptedBit = senderScrambler.ProcessBit(bit);
                    Bit decryptedBit = receiverScrambler.ProcessBit(encryptedBit);
                    encryptedBits.Add(encryptedBit);
                    decryptedBits.Add(decryptedBit);
                }
            }

            byte[] encryptedBytes = new BitArray(encryptedBits).ToByteArray();
            byte[] decryptedBytes = new BitArray(decryptedBits).ToByteArray();

            File.WriteAllBytes(EncryptedTextFilename, encryptedBytes);
            File.WriteAllBytes(DecryptedTextFilename, decryptedBytes);

            Console.ReadLine();
        }
    }
}
