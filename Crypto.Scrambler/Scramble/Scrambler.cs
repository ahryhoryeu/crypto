﻿using System.Linq;
using Crypto.Utils.Core;

namespace Scrambler.Scramble
{
    public class Scrambler
    {
        private readonly BitArray key;
        private BitArray register;

        public Scrambler(BitArray key, BitArray register)
        {
            this.key = key;
            this.register = register;
        }

        public Scrambler Clone()
        {
            return new Scrambler(key, register);
        }

        public Bit ProcessBit(Bit bit)
        {
            Bit toWrite = register.And(key).Aggregate(Bit.GetZero(), (x, y) => x + y);

            Bit shiftedBit;
            register = register.ShiftRightAndWrite(toWrite, out shiftedBit);

            return bit + shiftedBit;
        }
    }
}
