﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Crypto.Rsa.Core;
using Crypto.Utils.Core;

namespace Crypto.Rsa
{
    internal class Program
    {
        private const string SourceFileName = "exampleText.txt";
        private const string EncryptedTextFilename = "encrypted.txt";
        private const string DecryptedTextFilename = "decrypted.txt";

        private const int RsaKeyLength = 2048;

        private static void Main(string[] args)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var keyGenerator = new RsaKeyGenerator();
            var keyPair = keyGenerator.GenerateKeyPair(RsaKeyLength);
            stopWatch.Stop();
            Console.WriteLine("RSA key pair was generated. {0} elapsed", stopWatch.Elapsed);
            stopWatch.Reset();

            stopWatch.Start();
            var rsaTransformator = new RsaTransform();

            var bitBlockReader = new BitBlockReader(SourceFileName);
            bool hasMoreBlocks = true;
            var encryptedBits = new List<Bit>(bitBlockReader.OverallSize);
            var decryptedBits = new List<Bit>(bitBlockReader.OverallSize);

            while (hasMoreBlocks)
            {
                BitArray block;
                hasMoreBlocks = bitBlockReader.GetBlock(RsaKeyLength, out block);
                BitArray encryptedBlock = rsaTransformator.Transform(block, keyPair.PublicKey);
                BitArray decryptedBlock = rsaTransformator.Transform(encryptedBlock, keyPair.PrivateKey);
                encryptedBits.AddRange(encryptedBlock.Bits);
                decryptedBits.AddRange(decryptedBlock.Bits);
            }

            stopWatch.Stop();
            Console.WriteLine("Encrypting/decrypting finished, {0} elapsed", stopWatch.Elapsed);

            byte[] encryptedBytes = new BitArray(encryptedBits).ToByteArray();
            byte[] decryptedBytes = new BitArray(decryptedBits).ToByteArray();

            File.WriteAllBytes(EncryptedTextFilename, encryptedBytes);
            File.WriteAllBytes(DecryptedTextFilename, decryptedBytes);

            Console.ReadLine();
        }
    }
}
