﻿using System.Numerics;
using Crypto.Utils.Core;

namespace Crypto.Rsa.Core
{
    public class RsaTransform
    {
        public BigInteger Transform(BigInteger message, RsaKey key)
        {
            return BigInteger.ModPow(message, key.Exponent, key.Module);
        }

        public BitArray Transform(BitArray block, RsaKey key)
        {
            BigInteger blockInteger = new BigInteger(block.ToByteArray());
            BigInteger transformed = Transform(blockInteger, key);
            return new BitArray(transformed.ToByteArray());
        }
    }
}
