﻿namespace Crypto.Rsa.Core
{
    public class RsaKeyPair
    {
        public RsaKey PublicKey { get; set; }

        public RsaKey PrivateKey { get; set; }
    }
}
