﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using Crypto.Utils.BigNum;

namespace Crypto.Rsa.Core
{
    public class RsaKeyGenerator
    {
        public RsaKeyPair GenerateKeyPair(int bitLength)
        {
            while (true)
            {
                BigInteger p = GetPrimeNumber(bitLength/2);
                BigInteger q = GetPrimeNumber(bitLength/2);
                while (q == p)
                {
                    q = GetPrimeNumber(bitLength/2);
                }
                BigInteger n = p*q;
                BigInteger eiler = (p - 1)*(q - 1);
                BigInteger e = new BigInteger(Math.Pow(2, 16)) + 1; //open exponent = fifth Ferma number

                BigInteger d = e.ModInverse(eiler);
                if (d.Sign != -1)
                {
                    return new RsaKeyPair
                    {
                        PublicKey = new RsaKey
                        {
                            Exponent = e,
                            Module = n
                        },
                        PrivateKey = new RsaKey
                        {
                            Exponent = d,
                            Module = n
                        }
                    };
                }
            }
        }

        public BigInteger GetPrimeNumber(int bitLength)
        {
            RandomNumberGenerator random = RandomNumberGenerator.Create();
            var bytes = new byte[bitLength/8];
            random.GetBytes(bytes);
            var number = new BigInteger(bytes);
            var counter = 0;
            while (!number.IsPrimeMillerRabin((int) Math.Ceiling(Math.Log(bitLength, 2))))
            {
                random.GetBytes(bytes);
                number = new BigInteger(bytes);
                counter++;
            }

            return number;
        }
    }
}
