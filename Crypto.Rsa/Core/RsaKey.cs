﻿using System.Numerics;
using System.Runtime.Serialization;

namespace Crypto.Rsa.Core
{
    [DataContract]
    public struct RsaKey
    {
        [DataMember]
        public BigInteger Exponent { get; set; }
        [DataMember]
        public BigInteger Module { get; set; }
    }
}
