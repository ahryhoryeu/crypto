﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Crypto.Utils.Core
{
    public class RandomBitGenerator: IEnumerable<Bit>
    {
        private readonly Random random;

        public RandomBitGenerator()
        {
            random = new Random();
        }

        public RandomBitGenerator(int seed)
        {
            random = new Random(seed);
        }

        public BitArray GetRandomBitArray(int length)
        {
            var result = new Bit[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = GetRandomBit();
            }
            return new BitArray(result);
        }

        public Bit GetRandomBit()
        {
            double rand = random.NextDouble();
            return rand < 0.5
                ? Bit.GetZero()
                : Bit.GetOne();
        }

        public IEnumerator<Bit> GetEnumerator()
        {
            yield return GetRandomBit();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
