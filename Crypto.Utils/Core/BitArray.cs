﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Crypto.Utils.Core
{
    public class BitArray : IReadOnlyCollection<Bit>, IEquatable<BitArray>
    {
        public List<Bit> Bits { get; }

        public int Count => Bits.Count;

        public Bit this[int index] => Bits[index];

        public BitArray(IEnumerable<Bit> bits)
        {
            Bits = new List<Bit>(bits);
        }

        public BitArray(params BitArray[] bitArrays)
        {
            Bits = new List<Bit>();
            foreach (var bitArray in bitArrays)
            {
                Bits.AddRange(bitArray.Bits.ToList());
            }
        }

        public BitArray(System.Collections.BitArray bitArray)
        {
            Bits = bitArray.Cast<bool>().Select(b => new Bit(b)).ToList();
        }

        public BitArray(byte[] byteArray)
        {
            var bits = new System.Collections.BitArray(byteArray);
            Bits = bits.Cast<bool>().Select(b => new Bit(b)).ToList();
        }

        public BitArray(bool[] bits)
        {
            Bits = bits.Select(i => new Bit(i)).ToList();
        }

        public BitArray(int size)
        {
            Bits = new List<Bit>(size);
        }

        public static BitArray FromString(string bitString)
        {
            IEnumerable<Bit> bits = bitString.Select(Bit.FromChar);
            return new BitArray(bits);
        }

        public BitArray Xor(BitArray other)
        {
            return PerformBinaryBitwiseFunction(this, other, (x, y) => x + y);
        }

        public BitArray Or(BitArray other)
        {
            return PerformBinaryBitwiseFunction(this, other, (x, y) => x || y);
        }

        public BitArray And(BitArray other)
        {
            return PerformBinaryBitwiseFunction(this, other, (x, y) => x && y);
        }

        public BitArray ShiftRightAndWrite(Bit toWrite, out Bit shiftedBit)
        {
            var result = new Bit[Count];
            for (int i = 1; i < Count; i++)
            {
                result[i] = Bits[i - 1];
            }
            result[0] = toWrite;
            shiftedBit = Bits.Last();
            return new BitArray(result);
        }

        public BitArray Concat(BitArray concatenatedArray)
        {
            return new BitArray(this, concatenatedArray);
        }

        public BitArray AddRight(Bit bit)
        {
            var bits = Bits.ToList();
            bits.Add(bit);
            return new BitArray(bits);
        }

        public BitArray AddLeft(Bit bit)
        {
            var bits = new List<Bit> {bit};
            bits.AddRange(Bits);
            return new BitArray(bits);
        }

        public BitArray CyclicShiftLeft(int shift)
        {
            return Swap(shift);
        }

        public BitArray CyclicShiftRight(int shift)
        {
            return Swap(Bits.Count - shift);
        }

        public BitArray PeekBlock(int blockSize, int startIndex = 0)
        {
            if (Bits.Count >= blockSize + startIndex)
            {
                return new BitArray(Bits.Skip(startIndex).Take(blockSize));
            }
            throw new ArgumentOutOfRangeException(nameof(blockSize), "Block size is bigger than the array, or startIndex + blockSize is bigger than the array");
        }

        public BitArray PopBlock(int blockSize, out BitArray block, int startIndex = 0)
        {
            if (Bits.Count >= blockSize + startIndex)
            {
                var poppedBits = Bits.Skip(startIndex).Take(blockSize);
                block = new BitArray(poppedBits);
                return new BitArray(Bits.Skip(blockSize + startIndex));
            }
            throw new ArgumentOutOfRangeException(nameof(blockSize), "Block size is bigger than the array, or startIndex + blockSize is bigger than the array");
        }

        public static BitArray PerformBinaryBitwiseFunction(BitArray x, BitArray y, Func<Bit, Bit, Bit> function)
        {
            if (x.Count != y.Count)
            {
                throw new ArgumentException("Array must be of equal size");
            }
            var result = new Bit[x.Count];
            for (int i = 0; i < x.Count; i++)
            {
                result[i] = function(x[i], y[i]);
            }
            return new BitArray(result);
        }

        public BitArray PerformUnaryBitwiseFunction(Func<Bit, Bit> function)
        {
            return new BitArray(Bits.Select(function));
        }

        public byte[] ToByteArray()
        {
            if (Bits.Count%8 != 0)
            {
                return null;
            }

            System.Collections.BitArray systemBitArray = new System.Collections.BitArray(Bits.Select(b => b.Value).ToArray());
            byte[] result = new byte[Bits.Count / 8];
            systemBitArray.CopyTo(result, 0);
            return result;
        }

        public IEnumerator<Bit> GetEnumerator()
        {
            return Bits.GetEnumerator();
        }

        public BitArray Clone()
        {
            return new BitArray(Bits);
        }

        public bool Equals(BitArray other)
        {
            if (Count != other.Count)
            {
                return false;
            }
            return !Bits.Where((t, i) => t != other[i]).Any();
        }

        public override string ToString()
        {
            return string.Concat(Bits.Select(b => b.ToString()));
        }

        public BitArray ZeroExpansion(int toSize)
        {
            if (Count < toSize)
            {
                var bits = new List<Bit>();
                bits.AddRange(Bits);
                bits.AddRange(Enumerable.Repeat(Bit.Zero, toSize - Count));
                return new BitArray(bits);
            }
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public BitArray Swap(int pivot)
        {
            var leftPart = Bits.Skip(pivot).ToList();
            var rightPart = Bits.Take(pivot).ToList();

            return new BitArray(leftPart.Concat(rightPart));
        }
    }
}
