﻿using System;

namespace Crypto.Utils.Core
{
    public struct Bit : IEquatable<Bit>
    {
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Bit && Equals((Bit) obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public bool Value { get; }

        public Bit(bool value)
        {
            Value = value;
        }

        public Bit Opposite()
        {
            return new Bit(!Value);
        }

        public override string ToString()
        {
            return Value ? "1" : "0";
        }

        public static Bit FromChar(char bit)
        {
            if (bit != '0' && bit != '1')
            {
                throw new ArgumentOutOfRangeException(nameof(bit), "Char must be either '0' or '1'");
            }

            return new Bit(bit == '1');
        }

        public static readonly Bit Zero = new Bit(false);

        public static readonly Bit One = new Bit(true);

        public static Bit GetZero()
        {
            return new Bit(false);
        }

        public static Bit GetOne()
        {
            return new Bit(true);
        }

        bool IEquatable<Bit>.Equals(Bit other)
        {
            return Value == other.Value;
        }

        public static Bit operator +(Bit x, Bit y)
        {
            return new Bit(x.Value ^ y.Value);
        }

        public static Bit operator |(Bit x, Bit y)
        {
            return new Bit(x.Value || y.Value);
        }

        public static Bit operator &(Bit x, Bit y)
        {
            return new Bit(x.Value && x.Value);
        }

        public static bool operator true(Bit x)
        {
            return x.Value;
        }

        public static bool operator false(Bit x)
        {
            return x.Value;
        }

        public static implicit operator int (Bit x)
        {
            return x.Value ? 1 : 0;
        }

        public static bool operator ==(Bit x, Bit y)
        {
            return x.Value == y.Value;
        }

        public static bool operator !=(Bit x, Bit y)
        {
            return !(x == y);
        }
    }
}
