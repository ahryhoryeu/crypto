﻿using System;
using System.IO;

namespace Crypto.Utils.Core
{
    public class BitBlockReader
    {
        private BitArray bits;

        public int OverallSize => bits.Count;

        public BitBlockReader(string pathToFile)
        {
            var bytes = File.ReadAllBytes(pathToFile);
            var bitArray = new System.Collections.BitArray(bytes);
            bits = new BitArray(bitArray);
        }

        public bool GetBlock(int blockSize, out BitArray block)
        {
            try
            {
                bits = bits.PopBlock(blockSize, out block);
                return true;
            }
            catch (ArgumentOutOfRangeException)
            {
                block = bits;
                return false;
            }
        }
    }
}
