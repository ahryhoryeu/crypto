﻿using System;
using Crypto.Utils.Core;

namespace Crypto.Utils.Feistel
{
    public abstract class FeistelCell
    {
        protected abstract int BlockSize { get; }

        public virtual BitArray ProcessBlock(BitArray block, BitArray roundKey)
        {
            if (block.Count != BlockSize)
            {
                throw new ArgumentException($"Block size must be equal to {BlockSize}");
            }
            BitArray leftBlock = block.PeekBlock(BlockSize/2);
            BitArray rightBlock = block.PeekBlock(BlockSize/2, BlockSize/2);

            BitArray transformedRightBlock = TransformRightBlock(rightBlock, roundKey);
            BitArray resultRightBlock = transformedRightBlock.Xor(leftBlock);

            return rightBlock.Concat(resultRightBlock);
        }

        protected abstract BitArray TransformRightBlock(BitArray rightBlock, BitArray key);
    }
}
