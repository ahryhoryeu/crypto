﻿using System.Collections.Generic;
using System.Linq;
using Crypto.Utils.Core;

namespace Crypto.Utils.Feistel
{
    public abstract class FeistelNetwork<TCell> where TCell: FeistelCell, new()
    {
        protected TCell Cell { get; }
        protected abstract int RoundCount { get; }
        protected readonly BitArray Key;

        protected readonly IKeyExpander KeyExpander;

        protected FeistelNetwork(BitArray key, IKeyExpander keyExpander)
        {
            Cell = new TCell();
            Key = key;
            KeyExpander = keyExpander;
        }

        public virtual BitArray EncryptBlock(BitArray block)
        {
            return ProcessBlock(block, Enumerable.Range(0, RoundCount));
        }

        public virtual BitArray DecryptBlock(BitArray block)
        {
            return ProcessBlock(block, Enumerable.Range(0, RoundCount).Reverse());
        }

        protected virtual BitArray ProcessBlock(BitArray block, IEnumerable<int> rounds)
        {
            BitArray blockToProcess = block.Clone();
            BitArray processed =  rounds
                .Select(GetRoundKey)
                .Aggregate(blockToProcess, (current, roundKey) => Cell.ProcessBlock(current, roundKey));
            return processed.Swap(processed.Count/2);
        }

        protected virtual BitArray GetRoundKey(int roundNumber)
        {
            return KeyExpander.GetRoundKey(roundNumber);
        }
    }
}
