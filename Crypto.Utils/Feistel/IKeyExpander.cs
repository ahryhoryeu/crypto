﻿using Crypto.Utils.Core;

namespace Crypto.Utils.Feistel
{
    public interface IKeyExpander
    {
        BitArray GetRoundKey(int roundNumber);
    }
}