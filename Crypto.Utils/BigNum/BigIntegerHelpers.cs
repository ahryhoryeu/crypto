﻿using System.Numerics;
using System.Security.Cryptography;

namespace Crypto.Utils.BigNum
{
    public static class BigIntegerHelpers
    {
        public static bool IsPrimeMillerRabin(this BigInteger source, int certainty)
        {
            if (source == 2 || source == 3)
                return true;
            if (source < 2 || source % 2 == 0)
                return false;

            BigInteger d = source - 1;
            int s = 0;

            while (d % 2 == 0)
            {
                d /= 2;
                s += 1;
            }

            // There is no built-in method for generating random BigInteger values.
            // Instead, random BigIntegers are constructed from randomly generated
            // byte arrays of the same length as the source.
            RandomNumberGenerator rng = RandomNumberGenerator.Create();
            byte[] bytes = new byte[source.ToByteArray().LongLength];

            for (int i = 0; i < certainty; i++)
            {
                BigInteger a;
                do
                {
                    rng.GetBytes(bytes);
                    a = new BigInteger(bytes);
                }
                while (a < 2 || a >= source - 2);

                BigInteger x = BigInteger.ModPow(a, d, source);
                if (x == 1 || x == source - 1)
                    continue;

                for (int r = 1; r < s; r++)
                {
                    x = BigInteger.ModPow(x, 2, source);
                    if (x == 1)
                        return false;
                    if (x == source - 1)
                        break;
                }

                if (x != source - 1)
                    return false;
            }

            return true;
        }

        public static BigInteger ModInverse(this BigInteger source, BigInteger module)
        {
            BigInteger x, y;
            BigInteger gcd = ExtendedEuclide(source, module, out x, out y);
            if (gcd.Equals(BigInteger.One))
            {
                return x % module;
            }
            return BigInteger.MinusOne;
        }

        private static BigInteger ExtendedEuclide(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }
            BigInteger x1, y1;
            BigInteger gcd = ExtendedEuclide(b%a, a, out x1, out y1);
            x = y1 - (b/a)*x1;
            y = x1;
            return gcd;
        }
    }
}
