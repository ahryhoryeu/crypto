﻿using System.Text;
using Crypto.Caesar.Core;

namespace Crypto.Caesar.Process
{
    public abstract class CaesarProcesser
    {
        protected readonly Language Language;

        protected CaesarProcesser(Language language)
        {
            Language = language;
        }

        protected string ProcessText(string text, int key)
        {
            if (key < 0)
            {
                key = Language.Letters.Count + key;
            }

            var stringBuilder = new StringBuilder();
            foreach (char c in text)
            {
                char? r = null;
                if (char.IsLetter(c))
                {
                    r = char.IsUpper(c) ? ProcessCapital(c, key) : ProcessSmall(c, key);
                }
                stringBuilder.Append(r ?? c);
            }
            return stringBuilder.ToString();
        }

        protected abstract char ProcessSmall(char c, int key);

        protected virtual char ProcessCapital(char c, int key)
        {
            var small = char.ToLower(c);
            return char.ToUpper(ProcessSmall(small, key));
        }
    }
}
