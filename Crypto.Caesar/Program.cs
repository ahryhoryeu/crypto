﻿using System;
using System.IO;
using Crypto.Caesar.Break;
using Crypto.Caesar.Core;
using Crypto.Caesar.Decrypt;
using Crypto.Caesar.Encrypt;

namespace Crypto.Caesar
{
    class Program
    {
        private const string SourceFileName = "exampleText.txt";
        private const string EncryptedTextFilename = "encrypted.txt";
        private const string DecryptedTextFilename = "decrypted.txt";
        private const string BrokenTextFilename = "broken.txt";

        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the key");
            var key = int.Parse(Console.ReadLine());
            string originalText;
            using (StreamReader reader = File.OpenText(SourceFileName))
            {
                originalText = reader.ReadToEnd();
            }
            var language = new EnglishLanguage();
            var encryptor = new CaesarCipher(language);
            var decryptor = new CaesarDecryptor(language);
            var breaker = new CaesarBreaker(language, decryptor);

            string encryptedText = encryptor.Encrypt(originalText, key);
            string decryptedText = decryptor.Decrypt(encryptedText, key);

            bool successfulBreak;
            string brokenText = breaker.Break(encryptedText, out successfulBreak);

            WriteToFile(EncryptedTextFilename, encryptedText);

            WriteToFile(DecryptedTextFilename, decryptedText);

            WriteToFile(BrokenTextFilename, brokenText);

            Console.WriteLine("Break success status: {0}", successfulBreak);
            Console.ReadLine();
        }

        static void WriteToFile(string path, string text)
        {
            using (StreamWriter writer = new StreamWriter(path, false))
            {
                writer.Write(text);
            }
        }
    }
}
