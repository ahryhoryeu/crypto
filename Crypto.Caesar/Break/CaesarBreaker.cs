﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crypto.Caesar.Core;
using Crypto.Caesar.Decrypt;

namespace Crypto.Caesar.Break
{
    public class CaesarBreaker
    {
        private readonly Language language;
        private readonly CaesarDecryptor caesarDecryptor;

        public CaesarBreaker(Language language, CaesarDecryptor caesarDecryptor)
        {
            this.language = language;
            this.caesarDecryptor = caesarDecryptor;
        }

        public string Break(string ciphertext, out bool success)
        {
            IDictionary<char, double> ciphertextFreq = CalculateFrequencies(ciphertext);
            char mostPopularCharLanguage = GetMostPopularChar(language.Frequencies);
            char mostPopularCharCipher = GetMostPopularChar(ciphertextFreq);
            int shift = mostPopularCharCipher - mostPopularCharLanguage;

            IReadOnlyList<int> capitalLetterIndices = GetCapitalLetterIndices(ciphertext);

            string decrypted = caesarDecryptor.Decrypt(ciphertext, shift);
            IDictionary<char, double> decriptedFreq = CalculateFrequencies(decrypted);
            success = ChiSquareChecker.CheckCriteria(decriptedFreq, language);

            return decrypted;
        }

        private IDictionary<char, double> CalculateFrequencies(string text)
        {
            IReadOnlyList<char> letters = text.ToCharArray()
                                              .Where(char.IsLetter)
                                              .Select(char.ToLower)
                                              .ToList();

            return letters.GroupBy(c => c).ToDictionary(g => g.Key, g => Convert.ToDouble(g.Count())/letters.Count);
        }

        private char GetMostPopularChar(IDictionary<char, double> frequencyDictionary, int toSkip = 0)
        {
            return frequencyDictionary.OrderByDescending(kvp => kvp.Value).Skip(toSkip).First().Key;
        }

        private IReadOnlyList<int> GetCapitalLetterIndices(string text)
        {
            var indices = new List<int>();
            for (int i = 0; i < text.Length; i++)
            {
                if (char.IsLetter(text[i]) && char.IsUpper(text[i]))
                {
                    indices.Add(i);
                }
            }
            return indices;
        }

        private string RestoreCapitalLetters(string text, IReadOnlyList<int> indices)
        {
            char[] charArray = text.ToCharArray();
            foreach (int index in indices)
            {
                charArray[index] = char.ToUpper(charArray[index]);
            }
            return string.Concat(charArray);
        }
    }
}
