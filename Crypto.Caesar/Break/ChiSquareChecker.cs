﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crypto.Caesar.Core;

namespace Crypto.Caesar.Break
{
    public static class ChiSquareChecker
    {
        public static bool CheckCriteria(IDictionary<char, double> experimentalFrequencies, Language language)
        {
            IDictionary<char, double> actualFrequencies = language.Frequencies;

            var orderedActual =
                actualFrequencies.Where(kvp => experimentalFrequencies.ContainsKey(kvp.Key))
                    .OrderBy(kvp => kvp.Key)
                    .Select(kvp => kvp.Value)
                    .ToArray();

            var orderedExperimental = experimentalFrequencies.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value).ToArray();
            int n = orderedExperimental.Length;
            double chi = 0.0;

            for (int i = 0; i < n; i++)
            {
                chi += Math.Pow((orderedExperimental[i] - n*orderedActual[i]), 2)/(n*orderedActual[i]);
            }
            return chi < language.ChiSquare;
        }
    }
}
