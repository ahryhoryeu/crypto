﻿namespace Crypto.Caesar.Encrypt
{
    public interface ICipher
    {
        string Encrypt(string message, int key);
    }
}