﻿using Crypto.Caesar.Core;
using Crypto.Caesar.Process;

namespace Crypto.Caesar.Encrypt
{
    public class CaesarCipher: CaesarProcesser, ICipher
    {
        public CaesarCipher(Language language): base(language)
        {
        }

        public virtual string Encrypt(string message, int key)
        {
            return ProcessText(message, key);
        }

        protected override char ProcessSmall(char c, int key)
        {
            int originalLetterIndex = Language.Letters.IndexOf(c);
            int encryptedLetterIndex = (originalLetterIndex + key) % Language.Letters.Count;
            return Language.Letters[encryptedLetterIndex];
        }
    }
}
