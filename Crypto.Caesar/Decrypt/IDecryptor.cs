﻿namespace Crypto.Caesar.Decrypt
{
    public interface IDecryptor
    {
        string Decrypt(string ciphertext, int key);
    }
}
