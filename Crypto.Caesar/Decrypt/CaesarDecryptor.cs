﻿using Crypto.Caesar.Core;
using Crypto.Caesar.Process;

namespace Crypto.Caesar.Decrypt
{
    public class CaesarDecryptor: CaesarProcesser, IDecryptor
    {
        public CaesarDecryptor(Language language) : base(language)
        {
        }

        public string Decrypt(string ciphertext, int key)
        {
            return ProcessText(ciphertext, key);
        }

        protected override char ProcessSmall(char c, int key)
        {
            int originalLetterIndex = Language.Letters.IndexOf(c);
            int encryptedLetterIndex = originalLetterIndex - key;
            while (encryptedLetterIndex < 0)
            {
                encryptedLetterIndex += Language.Letters.Count;
            }
            return Language.Letters[encryptedLetterIndex];
        }
    }
}
