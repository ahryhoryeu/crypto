﻿namespace Crypto.Caesar.Core
{
    public class EnglishLanguage: Language
    {
        public override char AlphabetFirstLetter => 'a';
        public override char AlphabetLastLetter => 'z';
        public override double ChiSquare => 38.8851;
        protected override string LanguageName => "english";
    }
}
