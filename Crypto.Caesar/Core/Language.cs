﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Crypto.Caesar.ConfigSection;

namespace Crypto.Caesar.Core
{
    public abstract class Language
    {
        private IDictionary<char, double> frequencies;

        public abstract char AlphabetFirstLetter { get; }

        public abstract char AlphabetLastLetter { get; }

        public abstract double ChiSquare { get; }

        protected abstract string LanguageName { get; }

        protected FrequenciesConfiguration FrequenciesConfig
            => (FrequenciesConfiguration) ConfigurationManager.GetSection("frequencies");

        public IDictionary<char, double> Frequencies => frequencies ?? (frequencies = GetFrequencies());

        public IList<char> Letters => Frequencies.Keys.ToList();

        private IDictionary<char, double> GetFrequencies()
        {
            var language = FrequenciesConfig.Languages[LanguageName];
            return language.Letters.Cast<LetterElement>().ToDictionary(l => l.Letter, l => l.Frequency);
        }
    }
}
