﻿using System.Configuration;

namespace Crypto.Caesar.ConfigSection
{
    public class LanguageElement: ConfigurationElement
    {
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name => base["name"].ToString();

        [ConfigurationProperty("letters")]
        public LettersCollection Letters => (LettersCollection) base["letters"];
    }
}
