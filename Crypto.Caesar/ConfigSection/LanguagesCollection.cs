﻿using System.Configuration;

namespace Crypto.Caesar.ConfigSection
{
    [ConfigurationCollection(typeof(LanguageElement))]
    public class LanguagesCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LanguageElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LanguageElement) element).Name;
        }

        public new LanguageElement this[string name] => (LanguageElement)BaseGet(name);
    }
}
