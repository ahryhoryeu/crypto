﻿using System.Configuration;

namespace Crypto.Caesar.ConfigSection
{
    [ConfigurationCollection(typeof(LetterElement))]
    public class LettersCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LetterElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LetterElement) element).Letter;
        }
    }
}
