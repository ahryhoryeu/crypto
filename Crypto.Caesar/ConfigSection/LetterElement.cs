﻿using System;
using System.Configuration;

namespace Crypto.Caesar.ConfigSection
{
    public class LetterElement: ConfigurationElement
    {
        [ConfigurationProperty("letter", DefaultValue = "", IsKey = true, IsRequired = true)]
        public char Letter => Convert.ToChar(base["letter"]);

        [ConfigurationProperty("freq", DefaultValue = "0.0", IsKey = false, IsRequired = true)]
        public double Frequency => Convert.ToDouble(base["freq"]) / 100.0;
    }
}
