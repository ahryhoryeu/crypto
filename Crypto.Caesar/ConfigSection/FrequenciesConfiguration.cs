﻿using System.Configuration;

namespace Crypto.Caesar.ConfigSection
{
    public class FrequenciesConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("languages")]
        public LanguagesCollection Languages => (LanguagesCollection) base["languages"];
    }
}
