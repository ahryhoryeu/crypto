﻿using Crypto.Utils.Core;
using Crypto.Utils.Feistel;

namespace Crypto.Ghost.Core
{
    public class GhostKeyExpander: IKeyExpander
    {
        private const int KeyLength = 256;
        private const int RoundKeySize = 32;

        private readonly BitArray key;

        public GhostKeyExpander(BitArray key)
        {
            this.key = key;
        }

        public BitArray GetRoundKey(int roundNumber)
        {
            int keyNumber = roundNumber < 24 
                ? roundNumber % 8
                : 7 - roundNumber % 8;
            return key.PeekBlock(RoundKeySize, keyNumber * RoundKeySize);
        }
    }
}