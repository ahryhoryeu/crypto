﻿using System;
using Crypto.Utils.Core;

namespace Crypto.Ghost.Core
{
    public class GhostSBox
    {
        private static readonly int[][] SBoxes =
        {
            new[] { 12, 4, 6, 2, 10, 5, 11, 9, 14, 8, 13, 7, 0, 3, 15, 1 },
            new[] { 6, 8, 2, 3, 9, 10, 5, 12, 1, 14, 4, 7, 11, 13, 0, 15 },
            new[] { 11, 3, 5, 8, 2, 15, 10, 13, 14, 1, 7, 4, 12, 9, 6, 0 },
            new[] { 12, 8, 2, 1, 13, 4, 15, 6, 7, 0, 10, 5, 3, 14, 9, 11 },
            new[] { 7, 15, 5, 10, 8, 1, 6, 13, 0, 9, 3, 14, 11, 4, 2, 12 },
            new[] { 5, 13, 15, 6, 9, 2, 12, 10, 11, 7, 8, 1, 4, 3, 14, 0 },
            new[] { 8, 14, 2, 5, 6, 9, 1, 12, 15, 4, 11, 0, 13, 10, 3, 7 },
            new[] { 1, 7, 14, 13, 0, 5, 8, 3, 4, 15, 10, 6, 9, 12, 11, 2 }
        };

        public const int BlockSize = 4;

        public static BitArray TransformBlock(BitArray block, int sBoxNumber)
        {
            var number = Convert.ToInt32(block.ToString(), 2);
            var resultNumber = SBoxes[sBoxNumber][number];

            return BitArray.FromString(Convert.ToString(resultNumber, 2).PadLeft(4, '0'));
        }
    }
}