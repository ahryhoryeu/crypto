﻿using System.Collections.Generic;
using Crypto.Utils.Core;
using Crypto.Utils.Feistel;

namespace Crypto.Ghost.Core
{
    public class GhostCell: FeistelCell
    {
        protected override int BlockSize => 64;

        private const int CyclicShiftLength = 11;

        protected override BitArray TransformRightBlock(BitArray rightBlock, BitArray key)
        {
            BitArray xored = rightBlock.Xor(key);

            var sBoxedBlocks = new List<BitArray>(rightBlock.Count / GhostSBox.BlockSize);
            for (int i = 0; i < rightBlock.Count / GhostSBox.BlockSize; i++)
            {
                BitArray block = xored.PeekBlock(GhostSBox.BlockSize, i*GhostSBox.BlockSize);
                sBoxedBlocks.Add(GhostSBox.TransformBlock(block, i));
            }

            var sBoxedBlock = new BitArray(sBoxedBlocks.ToArray());

            return sBoxedBlock.CyclicShiftLeft(CyclicShiftLength);
        }
    }
}
