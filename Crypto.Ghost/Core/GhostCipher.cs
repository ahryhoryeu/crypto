﻿using Crypto.Utils.Core;
using Crypto.Utils.Feistel;

namespace Crypto.Ghost.Core
{
    public class GhostCipher: FeistelNetwork<GhostCell>
    {
        protected override int RoundCount => 32;

        public GhostCipher(BitArray key) : base(key, new GhostKeyExpander(key))
        {
        }
    }
}
