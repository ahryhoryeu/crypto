﻿using System.IO;
using System.Numerics;
using System.Runtime.Serialization.Json;
using Crypto.DigitalSignature.Core;
using Crypto.Rsa.Core;

namespace Crypto.DigitalSignature
{
    class Program
    {
        private const string SourceFileName = "exampleText.txt";
        private const string SignatureFileName = "exampleText.txt.sig";

        private const int RsaKeyLength = 2048;

        static void Main(string[] args)
        {
            var signatureManager=  new DigitalSignatureManager(new RsaTransform());
            var keyGenerator = new RsaKeyGenerator();
            var keyPair = keyGenerator.GenerateKeyPair(RsaKeyLength);

            var fileBytes = File.ReadAllBytes(SourceFileName);
            var signature = signatureManager.Sign(fileBytes, keyPair);

            var serializer = new DataContractJsonSerializer(typeof(Core.DigitalSignature), new DataContractJsonSerializerSettings
            {
                KnownTypes = new[] {typeof(RsaKey), typeof(BigInteger)}
            });

            using (FileStream stream = new FileStream(SignatureFileName, FileMode.Create))
            {
                serializer.WriteObject(stream, signature);
            }

            Core.DigitalSignature readSignature;
            using (FileStream stream = new FileStream(SignatureFileName, FileMode.Open))
            {
                readSignature = (Core.DigitalSignature) serializer.ReadObject(stream);
            }

            var result = signatureManager.CheckSignature(fileBytes, readSignature);
        }
    }
}
