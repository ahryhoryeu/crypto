﻿using System;
using System.Numerics;
using System.Runtime.Serialization;
using Crypto.Rsa.Core;

namespace Crypto.DigitalSignature.Core
{
    [DataContract]
    public class DigitalSignature
    {
        [DataMember]
        public BigInteger Signature { get; set; }
        [DataMember]
        public DateTime TimeStamp { get; set; }
        [DataMember]
        public RsaKey PublicKey { get; set; }
    }
}