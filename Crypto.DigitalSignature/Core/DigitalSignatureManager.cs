﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using Crypto.Rsa.Core;

namespace Crypto.DigitalSignature.Core
{
    public class DigitalSignatureManager
    {
        private readonly RsaTransform rsaTransform;

        public DigitalSignatureManager(RsaTransform rsaTransform)
        {
            this.rsaTransform = rsaTransform;
        }

        public DigitalSignature Sign(byte[] bytesToSign, RsaKeyPair keyPair)
        {
            var sha512 = SHA512.Create();
            var hash = sha512.ComputeHash(bytesToSign);
            var hashNumber = new BigInteger(hash);

            return new DigitalSignature
            {
                PublicKey = keyPair.PublicKey,
                TimeStamp = DateTime.Now,
                Signature = rsaTransform.Transform(hashNumber, keyPair.PrivateKey)
            };
        }

        public bool CheckSignature(byte[] document, DigitalSignature signature)
        {
            var sha512 = SHA512.Create();
            var hash = sha512.ComputeHash(document);
            var hashNumber = new BigInteger(hash);

            BigInteger decryptedSignature = rsaTransform.Transform(signature.Signature, signature.PublicKey);

            return hashNumber == decryptedSignature;
        }
    }
}