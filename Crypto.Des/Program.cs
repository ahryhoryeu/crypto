﻿using System.Collections.Generic;
using System.IO;
using Crypto.Des.Core;
using Crypto.Utils.Core;

namespace Crypto.Des
{
    class Program
    {
        private const string SourceFileName = "exampleText.txt";
        private const string EncryptedTextFilename = "encrypted.txt";
        private const string DecryptedTextFilename = "decrypted.txt";
        private const int BlockSize = 64;
        private const int KeyLength = 56;

        static void Main(string[] args)
        {
            var bitGenerator = new RandomBitGenerator();
            BitArray key = bitGenerator.GetRandomBitArray(KeyLength);

            var desCipher = new DesCipher(key);

            var bitBlockReader = new BitBlockReader(SourceFileName);
            bool hasMoreBlocks = true;
            var encryptedBits = new List<Bit>(bitBlockReader.OverallSize);
            var decryptedBits = new List<Bit>(bitBlockReader.OverallSize);

            while (hasMoreBlocks)
            {
                BitArray block;
                hasMoreBlocks = bitBlockReader.GetBlock(BlockSize, out block);
                if (hasMoreBlocks)
                {
                    BitArray encryptedBlock = desCipher.EncryptBlock(block);
                    BitArray decryptedBlock = desCipher.DecryptBlock(encryptedBlock);
                    encryptedBits.AddRange(encryptedBlock.Bits);
                    decryptedBits.AddRange(decryptedBlock.Bits);
                }
                else
                {
                    BitArray expandedBlock = block.ZeroExpansion(BlockSize);
                    int zeroesCount = expandedBlock.Count - block.Count;
                    BitArray encryptedBlock = desCipher.EncryptBlock(expandedBlock);
                    BitArray decryptedBlock = desCipher.DecryptBlock(encryptedBlock);
                    encryptedBits.AddRange(encryptedBlock.PeekBlock(BlockSize - zeroesCount).Bits);
                    decryptedBits.AddRange(decryptedBlock.PeekBlock(BlockSize - zeroesCount).Bits);
                }
            }

            byte[] encryptedBytes = new BitArray(encryptedBits).ToByteArray();
            byte[] decryptedBytes = new BitArray(decryptedBits).ToByteArray();

            File.WriteAllBytes(EncryptedTextFilename, encryptedBytes);
            File.WriteAllBytes(DecryptedTextFilename, decryptedBytes);
        }
    }
}
