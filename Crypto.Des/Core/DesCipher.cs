﻿using System;
using Crypto.Des.Core.KeyExpansion;
using Crypto.Utils.Core;
using Crypto.Utils.Feistel;

namespace Crypto.Des.Core
{
    public class DesCipher: FeistelNetwork<DesCell>
    {
        protected override int RoundCount => 16;

        public DesCipher(BitArray key): base(key, new DesKeyExpander(key))
        {
        }

        public override BitArray EncryptBlock(BitArray block)
        {
            return Process(block, base.EncryptBlock);
        }

        public override BitArray DecryptBlock(BitArray block)
        {
            return Process(block, base.DecryptBlock);
        }

        private BitArray Process(BitArray block, Func<BitArray, BitArray> processFunc)
        {
            BitArray initialPermutated = DesPermutator.PermutateBlock(block, DesPermutationTable.Initial);
            BitArray processed = processFunc(initialPermutated);
            return DesPermutator.PermutateBlock(processed, DesPermutationTable.Final);
        }
    }
}
