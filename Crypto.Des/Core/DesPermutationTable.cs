﻿namespace Crypto.Des.Core
{
    public enum DesPermutationTable
    {
        Function,
        Initial,
        Final
    }
}