﻿using System.Linq;
using Crypto.Utils.Core;
using Crypto.Utils.Feistel;

namespace Crypto.Des.Core.KeyExpansion
{
    public class DesKeyExpander: IKeyExpander
    {
        private readonly BitArray c0;
        private readonly BitArray d0;

        private readonly int[] roundShifts = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

        private readonly int[] selectorTable =
        {
            14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4,
            26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40,
            51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32
        };

        public DesKeyExpander(BitArray key)
        {
            BitArray expandedKey = InitialExpansion(key);
            BitArray permutated = DesKeyPermutator.Permutate(expandedKey);
            permutated = permutated.PopBlock(permutated.Count/2, out c0);
            d0 = permutated.Clone();
        }

        public BitArray GetRoundKey(int roundNumber)
        {
            int roundShift = roundShifts.Take(roundNumber+1).Sum();
            BitArray cRound = c0.CyclicShiftLeft(roundShift);
            BitArray dRound = d0.CyclicShiftLeft(roundShift);
            BitArray concatenated = cRound.Concat(dRound);

            var resultBits = new Bit[selectorTable.Length];
            for (int i = 0; i < selectorTable.Length; i++)
            {
                resultBits[i] = concatenated[selectorTable[i] - 1];
            }

            return new BitArray(resultBits);
        }

        private BitArray InitialExpansion(BitArray sourceKey)
        {
            var parts = new BitArray[8];
            for (int i = 0; i < 8; i++)
            {
                BitArray sourcePart = sourceKey.PeekBlock(7, i*7);
                Bit additional = sourcePart.Aggregate((b1, b2) => b1 + b2).Opposite();
                parts[i] = sourcePart.AddRight(additional);
            }

            return new BitArray(parts);
        }
    }
}
