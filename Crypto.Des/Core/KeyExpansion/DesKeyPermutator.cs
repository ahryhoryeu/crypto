﻿using Crypto.Utils.Core;

namespace Crypto.Des.Core.KeyExpansion
{
    public static class DesKeyPermutator
    {
        private static readonly int[] SelectionTable =
        {
            57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18,
            10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36,
            63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22,
            14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4
        };

        public static BitArray Permutate(BitArray key)
        {
            var bits = new Bit[key.Count - 8]; //TODO: refactor
            for (int i = 0; i < key.Count - 8; i++)
            {
                bits[i] = key[SelectionTable[i] - 1];
            }

            return new BitArray(bits);
        }
    }
}
