﻿using Crypto.Utils.Core;

namespace Crypto.Des.Core
{
    public static class DesBlockExpander
    {
        private const int BlockSize = 48;

        private static readonly int[] ExpandBlockTableRaw =
        {
            32, 1, 2, 3, 4, 5,
            4, 5, 6, 7, 8, 9,
            8, 9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32, 1
        };

        public static BitArray ExpandBlock(BitArray block)
        {
            var bits = new bool[BlockSize];
            for(int i = 0; i < BlockSize; i++)
            {
                bits[i] = block[ExpandBlockTableRaw[i] - 1].Value;
            }

            return new BitArray(bits);
        }
    }
}
