﻿using Crypto.Utils.Core;
using Crypto.Utils.Feistel;

namespace Crypto.Des.Core
{
    public class DesCell: FeistelCell
    {
        private readonly DesSBox sBox;

        protected override int BlockSize => 64;

        private const int SBoxBlockSize = 6;

        public DesCell()
        {
            sBox = new DesSBox();
        }

        protected override BitArray TransformRightBlock(BitArray rightBlock, BitArray key)
        {
            BitArray expanded = DesBlockExpander.ExpandBlock(rightBlock);

            BitArray sum = expanded.Xor(key);

            int blocksCount = sum.Count/SBoxBlockSize;
            var blocks = new BitArray[blocksCount];

            for (int i = 0; i < blocksCount; i++)
            {
                blocks[i] = sBox.PerformSBoxTransformation(sum.PeekBlock(SBoxBlockSize, i*SBoxBlockSize), i);
            }

            var sboxed = new BitArray(blocks);
            BitArray result = DesPermutator.PermutateBlock(sboxed, DesPermutationTable.Function);

            return result;
        }
    }
}